"""etlz - a lightweight and easy to use ETL library"""

# To dos:
# - finalize builder syntax
# - add full worker functionality back, test with simple identity
# - implement buffer
# - implement sort, join, filter, copy
# - add command line interface
# - implement extract/load: file, mysql, mongo, snowflake, http
# - implement missing transform operations
# - add http interface
# - add app structure

# ETL tips:
# - always order your records, and do it thoughtfully!
# - separate extract/transform/load

import itertools, functools, operator

class OperationWaiting(Exception):
    pass

def map_values(obj, func):
    return {k: func(v) for k, v in obj.items()}

# The etl framework is meant to be model agnostic, so for testing purposes,
# using the most basic implementation I can think of!
def dict_model(**fields):
    def instantiate(**values):
        return {k: fields[k](v) for k, v in values.items()}
    return instantiate

def create_selector_func(x, enum_choices=None):
    if   isinstance(x, str):
        func = lambda record: record[x]
    elif isinstance(x, int):
        func = lambda record: list(record.values())[x]
    elif isinstance(x, (tuple, list)):
        func = lambda record: [create_selector_func(s)(record) for s in x]
    elif isinstance(x, dict):
        func = lambda record: {
            k: create_selector_func(v)(record) for k, v in x}
    elif callable(x):
        func = x
    else:
        raise TypeError(
            'Cannot create selector function from type {}'.format(x.__class__))

    if enum_choices is not None:
        return lambda record: enum_choices.index(func(record))

    return func

class Operation(object):
    def __init__(self, models=[], to=None):
        self.models = models
        self.to = to or []

    def __lt__(self, secondary_from):
        return OperationWrapper(self, secondary_from=secondary_from)

    def __gt__(self, key):
        return OperationWrapper(self, key=key)
        
    def __or__(self, other):
        return Pipe([self, other])

    def connect(self, op):
        """Returns end operation in chain."""
        self.to.append(op)
        return op
    
    def resolve(self, saved_ops):
        return self
            
    def operate(self, record_batch):
        raise NotImplementedError

    def is_extracting(self):
        return False

    def tree(self):
        yield self
        for op in self.to:
            yield from op.tree()
            
    # def has_waiting_descendant(self, descendant):
    #     # NOTE: if nodes were doubly linked this might be quicker
    #     nodes = self.outputs.copy()
    #     while nodes:
    #         node, _ = nodes.pop()
    #         if node is descendant:
    #             return True
    #         if node.is_extracting():
    #             return False
    #         nodes.extend(node.outputs)
    #     return False

class Extract(Operation):
    def operate(self, record_batch_map):
        return (self.to, {0: self.send_batch()})

    def extract_batch(self):
        return [self.extract()]

    def extract(self):
        raise NotImplementedError

class Load(Operation):
    def operate(self, record_batch_map):
        record_batch = [v for k, v in sorted(record_batch_map.items())]
        self.load_batch(zip(*record_batch))
        return (self.to, {})

    def load_batch(self, record_batch):
        for records in record_batch:
            self.load(*records)

    def load(self, *records):
        raise NotImplementedError
    
class Transform(Operation):
    def operate(self, record_batch_map):
        if len(record_batch_map) < len(self.models):
            raise OperationWaiting()
        record_batch = [v for k, v in sorted(record_batch_map.items())]
        out_record_batch = zip(*self.transform_batch(zip(*record_batch)))
        out_record_batch_map = {
            i: [r for r in records if r is not None]
            for i, records in enumerate(out_record_batch)}
        return (self.to, out_record_batch_map)

    def transform_batch(self, record_batch):
        return [self.transform(*records) for records in record_batch]

    def transform(self, *records):
        raise NotImplementedError

class Identity(Transform):
    def transform(self, record):
        return (record,)

class Split(Transform):
    def __init__(self, field_selector, choices, **kwargs):
        super(Split, self).__init__(**kwargs)
        self.field_selector = create_selector_func(field_selector, choices)
        
    def transform(self, record):
        field_index = self.field_selector(record)
        out_record = [None] * (field_index+1)
        out_record[field_index] = record
        return tuple(out_record)

    def connect(self, op):
        self.to.append(op)
        return self

class Merge(Transform):
    def __init__(self, **kwargs):
        super(Merge, self).__init__(**kwargs)
    
    def transform_batch(self, record_batch):
        return [(r,) for records in record_batch for r in records]

class Buffer(Transform):
    def __init__(self, buffer_size, batch_size=None, **kwargs):
        super(Buffer, self).__init__(**kwargs)
        self.buffer_size = buffer_size
        self.buffer_ = []
        self.batch_size = batch_size        
        self._is_extracting = False

    def transform_batch(self, record_batch):
        if not self.is_extracting():
            if record_batch:
                self.buffer_.extend(record_batch)
                if not self.buffer_size or len(self.buffer_) < self.buffer_size:
                    return []
            self._is_extracting = True

        batch_size = self.batch_size or len(self.buffer_)
        batch = self.buffer_[:batch_size]
        self.buffer_ = self.buffer_[batch_size:]
        if not self.buffer_:
            self._is_extracting = False
        return batch

    def is_extracting(self):
        return self._is_extracting

class Sort(Buffer):
    def __init__(self, field, **kwargs):
        super(Sort, self).__init__(None, None, **kwargs)
        self.sort_by_field = field
    
    def transform_batch(self, record_batch):
        record_batch = super(Sort, self).transform_batch(record_batch)
        record_batch.sort(key=lambda records: records[0][self.sort_by_field])
        return record_batch        

class Join(Transform):
    def __init__(self, **kwargs):
        super(Join, self).__init__(**kwargs)
    
    def transform(self, primary_record, secondary_record):
        return (records[0],)   # TODO: implement this for real

class AutoMap(Transform):
    def __init__(self, mappings=[], to=[], **kwargs):
        super(AutoMap, self).__init__(to=to, **kwargs)
        self.mappings = [
            map_values(
                dict(self._default_mapping(op), **(mapping or {})),
                create_selector_func)
            for mapping, op in itertools.zip_longest(mappings, to)]

    @staticmethod
    def _default_mapping(op):
        return {}
        
    def transform(self, *records):
        return tuple(
            {k: f_mapper(record) for k, f_mapper in mapping.items()}
            for mapping, record in zip(self.mappings, records))

class OperationWrapper(object):
    def __init__(self, op, secondary_from=[], key=None):
        self.op = op
        self.key = key or ''
        self.secondary_from = [str(k or '') for k in secondary_from]
        
    def __or__(self, other):
        return Pipe([self, other])

    def resolve(self, saved_ops):
        for k in self.secondary_from:
            saved_ops[k].connect(self.op)
            
        saved_ops[self.key] = self.op
        
        return self.op
        
class Pipe(object):
    def __init__(self, ops=None):
        self.ops = ops

    def resolve(self, saved_ops=None):
        if saved_ops is None:
            saved_ops = {}
        last_op = self.ops[0]
        for op in self.ops[1:]:
            last_op = last_op.connect(op.resolve(saved_ops))
        return self.ops[0]

    def __or__(self, other):
        return Pipe(self.ops+[other])

    def __str__(self):
        return str(self.ops)

class Etl(object):
    def __init__(self):
        self.pipes = []

    def __ior__(self, pipe):
        if not isinstance(pipe, Pipe):
            raise TypeError('Must be a Pipe')

        self.pipes.append(pipe)
        
        return self

    def resolve(self):
        saved_ops = {}
        return [pipe.resolve(saved_ops) for pipe in self.pipes]

    def __call__(self, *records_list, **options):
        # root.operate(args) if no args, transform, load!!
        return [None for root, args in zip(self.resolve(), records_list)]
