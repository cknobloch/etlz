import unittest

from etlz import (
    dict_model,
    OperationWaiting, Operation, Extract, Load, Transform,
    Identity, Split, Merge, AutoMap, Buffer, Sort,
    OperationWrapper, Pipe, Etl,
)

class ModelTests(unittest.TestCase):
    def test_dict_model(self):
        AModel = dict_model(a=str, b=int)
        a_model = AModel(a=1, b='2')
        self.assertEqual(a_model, {'a': '1', 'b': 2})

class OperationTests(unittest.TestCase):
    def test_tree(self):
        c = Operation()
        b = Operation(to=[c])
        a = Operation(to=[b])
        self.assertEqual(list(a.tree()), [a, b, c])

    def test_operate_empty(self):
        record_batch_map = {
            0: [],
        }
        identity = Identity(models=[])
        actual_record_batch_map = identity.operate(record_batch_map)
        self.assertEqual(
            actual_record_batch_map,
            ([], {}))

    def test_operate_one(self):
        record_batch_map = {
            0: [1],
        }
        identity = Identity(models=[int])
        actual_record_batch_map = identity.operate(record_batch_map)
        self.assertEqual(
            actual_record_batch_map,
            ([], {0: [1]}))

    def test_operate_sparse_output(self):
        class TestTransform(Transform):
            def transform(self, record):
                return (None, None, record, None)
        record_batch_map = {
            0: ['foo'],
        }
        transform = TestTransform(models=[str])
        actual_record_batch_map = transform.operate(record_batch_map)
        self.assertEqual(
            actual_record_batch_map,
            ([], {0: [], 1: [], 2: ['foo'], 3: []}))

    def test_operate_missing_input(self):
        record_batch_map = {
            0: [],
        }
        a_model = dict_model(a=int)(a=1)
        b_model = dict_model(b=str)(b='2')
        identity = Identity(models=[a_model, b_model])
        self.assertRaises(
            OperationWaiting,
            lambda: identity.operate(record_batch_map))

class LoadTests(unittest.TestCase):
    def test_load(self):
        class TestLoad(Load):
            loaded_record = None
            def load(self, record):
                self.loaded_record = record
        load = TestLoad(models=[dict_model(a=int)])
        load.operate({0: [{'a': 1}]})
        self.assertEqual(load.loaded_record, {'a': 1})
        
class TransfomerTests(unittest.TestCase):
    def test_transform_batch(self):
        identity = Identity()
        records_batch = [(1,), (4,)]
        self.assertEqual(identity.transform_batch(records_batch), records_batch)

    def test_transform(self):
        identity = Identity()
        records = (1,)
        self.assertEqual(identity.transform(*records), records)

    def test_automapper_empty(self):
        mapper = AutoMap([], [None])
        a_record, b_record, c_record = {'a': 1}, {'b': 2}, {'c': 3}
        records = (a_record, b_record, c_record)
        self.assertEqual(mapper.transform(*records), ({},))

    def test_automapper_positional_map(self):
        mapper = AutoMap([{'bbb': 1}], [None])
        a_record = {'a': 'A', 'b': 'B', 'c': 'C'}
        self.assertEqual(mapper.transform(a_record), ({'bbb': 'B'},))
                
    def test_automapper_one_of_three(self):
        mapper = AutoMap(
            [{'aaa': 'a'}], [None])
        a_record, b_record, c_record = {'a': 1}, {'b': 2}, {'c': 3}
        records = (a_record, b_record, c_record)
        self.assertEqual(
            mapper.transform(*records),
            ({'aaa': 1},))

    def test_automapper_all_records(self):
        mapper = AutoMap(
            [
                {'aaa': 'a'},
                {'bbb': 'b'},
                {'ccc': 'c'}
            ],
            [None],)
        a_record, b_record, c_record = {'a': 1}, {'b': 2}, {'c': 3}
        records = (a_record, b_record, c_record)
        self.assertEqual(
            mapper.transform(*records),
            ({'aaa': 1}, {'bbb': 2}, {'ccc': 3}))

    def test_automapper_lambda(self):
        mapper = AutoMap(
            [
                {'abc': lambda r: (r['a'] + r['b']) / r['c']}
            ],
            [None])
        abc_record = {'a': 1, 'b': 2, 'c': 3}
        records = (abc_record,)
        self.assertEqual(
            mapper.transform(*records),
            ({'abc': 1},))

    def test_split(self):
        split = Split('a', ['x', 'y'])
        self.assertEqual(
            split.transform_batch([({'a': 'y'},), ({'a': 'x'},)]),
            [(None, {'a': 'y'}), ({'a': 'x'},)])

    def test_merge(self):
        merge = Merge()
        self.assertEqual(
            merge.transform_batch([({'a': 1}, {'a': 2})]),
            [({'a': 1},), ({'a': 2},)])

    def test_buffer(self):
        buffer_ = Buffer(4, 2)
        self.assertEqual(
            buffer_.transform_batch([({'a': 1},)]),
            [])
        self.assertEqual(
            buffer_.transform_batch([({'a': 2},)]),
            [])
        self.assertEqual(
            buffer_.transform_batch([({'a': 3},)]),
            [])
        self.assertEqual(
            buffer_.transform_batch([({'a': 4},)]),
            [({'a': 1},), ({'a': 2},)])
        self.assertEqual(
            buffer_.transform_batch([]),
            [({'a': 3},), ({'a': 4},)])
        self.assertEqual(
            buffer_.transform_batch([]),
            [])

    def test_sort(self):
        sort = Sort('a')
        self.assertEqual(
            sort.transform_batch([
                ({'a': 3, 'b': 1},),
                ({'a': 2, 'b': 2},),
                ({'a': 1, 'b': 3},)
            ]),
            [])
        self.assertEqual(
            sort.transform_batch([]),
            [
                ({'a': 1, 'b': 3},),
                ({'a': 2, 'b': 2},),
                ({'a': 3, 'b': 1},)
            ])
        
class PipeTests(unittest.TestCase):
    def test_pipe_connect_one_to_one(self):
        a_identity = Identity()
        b_identity = Identity()
        actual = a_identity | b_identity
        actual_first_op = actual.resolve()
        self.assertEqual(actual.__class__, Pipe)
        self.assertEqual(actual.ops, [a_identity, b_identity])
        self.assertEqual(actual_first_op.to, [b_identity])

    def test_pipe_connect_one_to_many(self):
        split = Split('x', ['1', '2', '3'])
        a_identity = Identity()
        b_identity = Identity()
        c_identity = Identity()
        actual = split | a_identity | (b_identity | c_identity)
        actual_first_op = actual.resolve()
        self.assertEqual(actual.__class__, Pipe)
        self.assertEqual(actual.ops[0], split)
        self.assertEqual(actual.ops[1], a_identity)
        self.assertEqual(actual.ops[2].__class__, Pipe)
        self.assertEqual(actual.ops[2].ops, [b_identity, c_identity])
        self.assertEqual(actual_first_op.to, [a_identity, b_identity])
        self.assertEqual(actual.ops[1].to, [])
        self.assertEqual(actual.ops[2].ops[1].to, [])

    def test_pipe_connect_many_to_one(self):
        split = Split('x', ['1', '2', '3'])
        a_identity = Identity()
        b_identity = Identity()
        c_identity = Identity()
        d_identity = Identity()
        merge = Merge()        
        actual = (split
            | (a_identity > 'foo')
            | (b_identity > 'bar')
            | (c_identity | (merge < ('foo', 'bar')) | d_identity))
        actual_first_op = actual.resolve()
        self.assertEqual(actual.__class__, Pipe)
        self.assertEqual(actual.ops[0], split)        
        self.assertEqual(actual.ops[1].__class__, OperationWrapper)
        self.assertEqual(actual.ops[1].op, a_identity)
        self.assertEqual(actual.ops[2].__class__, OperationWrapper)
        self.assertEqual(actual.ops[2].op, b_identity)
        self.assertEqual(actual.ops[3].__class__, Pipe)
        self.assertEqual(actual.ops[3].ops[0], c_identity)
        self.assertEqual(actual.ops[3].ops[1].__class__, OperationWrapper)
        self.assertEqual(actual.ops[3].ops[1].op, merge)
        self.assertEqual(actual.ops[3].ops[2], d_identity)

    def test_etl(self):
        a_identity = Identity()
        b_identity = Identity()
        c_identity = Identity()
        d_identity = Identity()
        etl = Etl()
        etl |= a_identity | b_identity
        etl |= c_identity | d_identity
        self.assertEqual(etl.resolve(), [a_identity, c_identity])
